<!DOCTYPE html>

<html>
    <head>
<script>
function hideMsg(success)
{
	var elem = document.getElementById(success);
	elem.parentNode.removeChild(elem);
	if (document.getElementsByClassName('msg-box').length === 0)
	{
		var arr = document.getElementsByClassName('underlay');
		for (var i = 0; i < arr.length; ++i)
			arr[i].parentNode.removeChild(arr[i]);
	}
}	
</script>
	<style>
.error {
  border: 2px solid red;
}
    </style>
        <title>Сайт с формой (вау)</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body class="bg-blue text-white font-roboto">
	
<?php
header('Content-Type: text/html; charset=UTF-8');
if (!empty($messages)) {
  print('<div id="messages">');
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
if ($save)
{
	print('<div class="underlay">
		   <div class="msg-box round shadow bg-blue" id="success" style="padding: 10px;">
				<h1 class="p centered-text">Данные сохранены</h1>
				<p class="p">Введенные данные доступны в личном кабинете, данные для входа:</p>
				<h3 class="ppp">Логин: ' . $user . '</p>
				<h3 class="ppp">Пароль: ' . $pass . '</p>
				<button style="width: 100%;" onclick="hideMsg(\'success\')">Ясно</button>
			</div>
			</div>');
}
?>
		<form action="login.php" style="width: auto; padding: 5px;" class="bg-purple">
			<?php
				if (empty($_SESSION['login']))
					print('<input type="submit" value="Войти в кабинет"/>');
				else
					print('Здравствуйте, ' .$_SESSION['login'] .'. <input type="submit" value="Выйти"/>');
			?>
		</form>
		<?php
			if (!empty($_SESSION['admin']) && $_SESSION['admin'])
				print('<form style="width: auto; padding: 5px;" class="bg-purple" action="users.php"><input type="submit" value="Просмотр данных пользователей"/></form>');
		?>
        <div class="content bg-yellow">
            <form class="round centered bg-purple p shadow" action="index.php" method="POST">
                <ul class="ul-clear centered">
                    <li class="p">
                        <label for="name-field">Имя</label><br>
                        <input type="text" class="round <?php if ($errors_arr['fio']) {print 'error';} ?>" name="name" value="<?php print $values['fio']; ?>" id="name-field">
                    </li>
                    <li class="p">
                        <label for="mail-field">Электронная почта</label><br>
                        <input type="email" class="round <?php if ($errors_arr['mail']) {print 'error';} ?>" name="mail" id="mail-field" value="<?php print $values['mail']; ?>"/>
                    </li>
                    <li class="p">
                        <label for="birth-field">Дата рождения</label><br>
                        <input type="date" class="round" name="birthday" value="<?php print $values['birth']; ?>" id="birth-field"/>
                    </li>
                    <li class="p">
                    <label for="sex-field">Пол</label>
                        <div id="sex-field">
                            <label for="male-radio">Мужской</label>
                            <input type="radio" name="sex" id="male-radio" checked="checked" value="male"/>
                            
                            <label for="fem-radio">Женский</label>
                            <input type="radio" name="sex" id="fem-radio" value="female"/>
                        </div>
                    </li>
                    <li class="p">
                        <label for="limb-field">Количество конечностей</label>
                        <div id="limb-field">
                            <ul class="ul-clear">
                                <li>
                                    <label for="one-limb">1</label>
                                    <input type="radio" name="limbs" id="one-limb" checked="checked" value="1"/>
                                </li>
                                <li>
                                    <label for="two-limb">2</label>
                                    <input type="radio" name="limbs" id="two-limb" value="2"/>
                                </li>
                                <li>
                                    <label for="three-limb">3</label>
                                    <input type="radio" name="limbs" id="three-limb" value="3"/>
                                </li>
                                <li>
                                    <label for="four-limb">4</label>
                                    <input type="radio" name="limbs" id="four-limb" value="4"/>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="p">
                        <label for="specs-list">Сверхспособности</label><br>
                        <select multiple name="specs" <?php if ($errors_arr['fio']) {print 'class="error"';} ?> >
                            <option value="godmode" <?php if ($values['specs'] == 'godmode') print('selected="selected"'); ?> >Бессмертие</option>
                            <option value="fly" <?php if ($values['specs'] == 'fly') print('selected="selected"'); ?>>Левитация</option>
                            <option value="noclip" <?php if ($values['specs'] == 'noclip') print('selected="selected"'); ?>>Проходить сквозь стены</option>
                        </select>
                    </li>
                    <li class="p">
                        <label for="bio">Биография</label><br>
                        <textarea class="round" id="bio" style="resize: none;" name="biography"><?php print $values['bio']; ?></textarea>
                    </li>
					<?php
					if (empty($_SESSION['login']))
						print('<li class="p">
                        <label for="check-admin">Хочу стать админом</label>
                        <input type="checkbox" id="check-admin" name="isadmin" value="true">
                    </li>');
					?>
                    <li class="p">
                        <label for="check-contract">С контрактом ознакомлен</label>
                        <input type="checkbox" id="check-contract" name="agree" value="true">
                    </li>
                    <li class="p">
                        <input type="submit" value="Отправить форму"/>
                    </li>
            </ul>
            </form>
        </div>
    </body>
</html>