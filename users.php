﻿<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
	$user = 'u20331';
    $pass = '9559325';
    $db = new PDO('mysql:host=localhost;dbname=u20331', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
	$stmt = $db->prepare("SELECT * FROM application");
	$stmt->execute();
	$dt = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
	  <link rel="stylesheet" type="text/css" href="style.css">
	  <meta charset="utf-8">
	  <title>Пользователи</title>
	</head>
	<body class="font-roboto bg-blue">
		<div class="content bg-yellow round shadow">
		<form action="process.php" method="get">
			<table class="users-table bg-purple text-white">
				<tr>
					<th>Имя пользователя</th>
					<th>Имя</th>
					<th>Адрес электронной почты</th>
					<th>Дата рождения</th>
					<th>Пол</th>
					<th>Количество конечностей</th>
					<th>Сверхспособность</th>
					<th>Биография</th>
					<th>Удалить</th>
				</tr>
				<?php
					for ($i = 0; $i < count($dt); ++$i)
					{	
						print('<tr>');
						print('<td>' . $dt[$i]['username'] . '</td>');
						print('<td><input type="text" name="name'.$i.'" pattern="^[A-Z]?[a-z][a-z]*(?:\s[A-z]?[a-z][a-z]*)*$" value="' . $dt[$i]['name'] . '"/></td>');
						print('<td><input type="email" name="email'.$i.'" value="' . $dt[$i]['mail'] . '"/></td>');
						print('<td><input type="date" name="date'.$i.'" value="' . $dt[$i]['birthday'] . '"/></td>');
						print('<td>');
							print('М');
							print('<input type="radio" value="male" name="sex'.$i.'" '. ($dt[$i]['sex'] == 'male' ? 'checked="checked"' : '') .'/>');
							print('Ж');
							print('<input type="radio" value="female" name="sex'.$i.'" '. ($dt[$i]['sex'] == 'female' ? 'checked="checked"' : '') .'/>');
						print('</td>');
						print('<td><input type="number" name="limbs'.$i.'" min="1" max="4" value="'.$dt[$i]['limbs'].'"/> </td>');
						
						print('<td>');
							print('<select name="specs'.$i.'">
										<option value="godmode" '.($dt[$i]['specs'] == 'godmode' ? 'selected="selected"' : '').'>Бессмертие</option>
										<option value="fly" '.($dt[$i]['specs'] == 'fly' ? 'selected="selected"' : '').'>Левитация</option>
										<option value="noclip" '.($dt[$i]['specs'] == 'noclip' ? 'selected="selected"' : '').'>Проходить сквозь стены</option>
								   </select>');
						print('</td>');
						
						print('<td>');
							print('<textarea style="resize: none;" name="biography'.$i.'">'.$dt[$i]['bio'].'</textarea>');
						print('</td>');
						
						if ($dt[$i]['username'] == $_SESSION['login'])
							print('<td>Это вы</td>');
						else
							print('<td><input type="checkbox" name="delete'. $i . '" value="delete"/></td>');
						print('</tr>');
					}
				?>
			</table>
			<input type="submit" style="margin: 5px;" value="Сохранить изменения" />
		</form>
		<form action="index.php">
			<input type="submit" style="margin: 5px;" value="Назад" />
		</form>
		</div>
	</body>
</html>
<?php
}
?>