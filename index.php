<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
	session_start();
	 
    $messages = array();
	$pass = empty($_COOKIE['pass']) ? '' : $_COOKIE['pass'];
	$user = empty($_COOKIE['user']) ? '' : $_COOKIE['user'];
	$save = false;
	$error = false;

    if (!empty($_COOKIE['save'])) {
		setcookie('save', '', 100000);
		setcookie('pass', '', 100000);
		setcookie('user', '', 100000);
		$save = true;
    }
	
	if (!empty($_COOKIE['override'])) {
		setcookie('override', '', 100000);
		setcookie('pass', '', 100000);
		setcookie('user', '', 100000);
		$messages[]="Данные обновлены.";
    }

    $errors_arr = array();
    $errors_arr['fio'] = !empty($_COOKIE['fio_error']);
    $errors_arr['mail'] = !empty($_COOKIE['mail_error']);
    $errors_arr['birth'] = !empty($_COOKIE['birth_error']);
    $errors_arr['specs'] = !empty($_COOKIE['specs_error']);

    if ($errors_arr['fio']) {
        setcookie('fio_error', '', 100000);
		$messages[] = '<div class="error">Заполните имя латиницей вида Имя Фамилия.</div>';
		$error = true;
    }

    if ($errors_arr['mail']) {
        setcookie('mail_error', '', 100000);
		$messages[] = '<div class="error">Заполните адрес почты.</div>';
		$error = true;
    }

    if ($errors_arr['birth']) {
        setcookie('birth_error', '', 100000);
		$messages[] = '<div class="error">Заполните дату рождения.</div>';
		$error = true;
    }


    if ($errors_arr['specs']) {
        setcookie('specs_error', '', 100000);
		$messages[] = '<div class="error">Выберете хотя бы одну способность.</div>';
		$error = true;
    }
	
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['mail'] = empty($_COOKIE['mail_value']) ? '' : $_COOKIE['mail_value'];
    $values['birth'] = empty($_COOKIE['birth_value']) ? '' : $_COOKIE['birth_value'];
    $values['specs'] = empty($_COOKIE['specs_value']) ? '' : $_COOKIE['specs_value'];
	$values['bio'] =  empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];

	
	if (!$error && !empty($_SESSION['login']) && !empty($_COOKIE[session_name()]))
	{
	  try 
	  {
		$user = 'u20331';
        $pass = '9559325';
        $db = new PDO('mysql:host=localhost;dbname=u20331', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		$stmt = $db->prepare('SELECT name, mail, birthday, sex, limbs, specs, bio FROM application WHERE username=:log');
		$stmt->bindParam(':log', $_SESSION['login']);
		$stmt->execute();
		$db_data = $stmt->fetchAll();
		
		$values['fio'] = $db_data[0]['name'];
		$values['mail'] = $db_data[0]['mail'];
		$values['birth'] = $db_data[0]['birthday'];
		$values['specs'] = $db_data[0]['specs'];
		$values['bio'] = $db_data[0]['bio'];
	  }
	  catch(PDOException $e)
	  {
		 $messages[] = ('Error :' . $e->getMessage());
         exit();
	  }
    }
	
    include('form.php');
} else {
	
    $errors = FALSE;
    $fio_pattern = '/^[A-Z]?[a-z][a-z]*(?:\s[A-z]?[a-z][a-z]*)*$/';
    $mail_pattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

    if (!preg_match($fio_pattern, $_POST['name'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
		setcookie('fio_value', $_POST['name'], time() + 30 * 24 * 60 * 60 * 12);
    }

    if (!preg_match($mail_pattern, $_POST['mail'])) {
        setcookie('mail_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
		setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60 * 12);
    }

    if (empty($_POST['birthday'])) {
        setcookie('birth_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
		setcookie('birth_value', $_POST['birthday'], time() + 30 * 24 * 60 * 60 * 12);
    }

    if (empty($_POST['specs'])) {
        setcookie('specs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
		setcookie('specs_value', $_POST['specs'], time() + 30 * 24 * 60 * 60 * 12);
    }
	
	setcookie('bio_value', $_POST['biography'], time() + 30 * 24 * 60 * 60 * 12);

    if ($errors == TRUE || empty($_POST['agree'])){
	header('Location: index.php');
        exit();
    }else
    if (!empty($_POST['agree'])) {
       		setcookie('fio_error', '', 100000);
			setcookie('mail_error', '', 100000);
			setcookie('birth_error', '', 100000);
			setcookie('specs_error', '', 100000);
    }

    try {
        $user = 'u20331';
        $pass = '9559325';
        $db = new PDO('mysql:host=localhost;dbname=u20331', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        
		if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
			$stmt = $db->prepare("UPDATE application SET name=:name, mail=:mail, birthday=:birthday, sex=:sex, limbs=:limbs, specs=:specs, bio=:bio WHERE username=:username");
			$stmt->bindParam(':username', $un);
			$stmt->bindParam(':name', $name);
			$stmt->bindParam(':mail', $mail);
			$stmt->bindParam(':birthday', $birthday);
			$stmt->bindParam(':sex', $sex);
			$stmt->bindParam(':limbs', $limbs);
			$stmt->bindParam(':specs', $specs);
			$stmt->bindParam(':bio', $bio);
			
			$un = strip_tags($_SESSION['login']);
			$db->quote($un);
			$name = strip_tags($_POST['name']);
			$mail = strip_tags($_POST['mail']);
			$birthday = $_POST['birthday'];
			$sex = $_POST['sex'];
			$limbs = intval($_POST['limbs']);
			$specs = $_POST['specs'];
			$bio = strip_tags($_POST['biography']);
			
			$stmt->execute();
			setcookie('override', '1');
		}
		else
		{
			$stmt = $db->prepare("INSERT INTO application (username, name, mail, birthday, sex, limbs, specs, bio) VALUES (:username, :name, :mail, :birthday, :sex, :limbs, :specs, :bio)");
			$stmt->bindParam(':username', $username);
			$stmt->bindParam(':name', $name);
			$stmt->bindParam(':mail', $mail);
			$stmt->bindParam(':birthday', $birthday);
			$stmt->bindParam(':sex', $sex);
			$stmt->bindParam(':limbs', $limbs);
			$stmt->bindParam(':specs', $specs);
			$stmt->bindParam(':bio', $bio);
		
			$username = ($_POST['isadmin'] ? 'a' : 'u') . crc32(strtolower(str_replace(' ', '', $_POST['name'])) . strval(rand()));
			$password = substr(md5($username), 0, 5);
		
			$name = strip_tags($_POST['name']);
			$mail = strip_tags($_POST['mail']);
			$birthday = strip_tags($_POST['birthday']);
			$sex = $_POST['sex'];
			$limbs = intval($_POST['limbs']);
			$specs = $_POST['specs'];
			$bio = strip_tags($_POST['biography']);
		
			$stmt->execute();
			
			if (!$_POST['isadmin'])
			{
				$stmt = $db->prepare("INSERT INTO users (username, password) VALUES (:username, :password)");
				$stmt->bindParam(':username', $username);
				$stmt->bindParam(':password', $password);
				$stmt->execute();
			}
			else
			{
				$stmt = $db->prepare("INSERT INTO admins (username, password) VALUES (:username, :password)");
				$stmt->bindParam(':username', $username);
				$stmt->bindParam(':password', $password);
				$stmt->execute();
			}
		
			setcookie('save', '1');
		}
		
		setcookie('user', $username, time() + 24 * 60 * 60);
		setcookie('pass', $password, time() + 24 * 60 * 60);
		header('Location: ./');
    }
    catch(PDOException $e)
    {
        print('Error :' . $e->getMessage());
        exit();
    }
}

?>