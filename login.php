<?php
header('Content-Type: text/html; charset=UTF-8');

session_start();

if (!empty($_SESSION['login']))
{
	session_destroy();
	header('Location: ./');
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<link rel="stylesheet" type="text/css" href="style.css">

<body class="bg-blue text-white font-roboto">
  <div class="content bg-yellow">
    <form class="round centered bg-purple p shadow" action="" method="POST">
      <ul class="ul-clear centered">
        <li class="p">
          <label for="name-field">Логин</label>
          <br>
          <input type="text" class="round" name="login" id="name-field">
        </li>
        <li class="p">
          <label for="pass-field">Пароль</label>
          <br>
          <input type="text" class="round" id="pass-field" name="password">
        </li>
        <li class="p">
          <input type="submit" value="Войти" />
        </li>
      </ul>
    </form>
  </div>
</body>

<?php
}
else {
	try 
	{
		$user = 'u20331';
		$pass = '9559325';
		$db = new PDO('mysql:host=localhost;dbname=u20331', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		$stmt = $db->prepare('(SELECT users.password FROM users WHERE username=:un) UNION (SELECT admins.password FROM admins WHERE username=:un)');
		$stmt->bindParam(':un', $un);
		$un = strip_tags($_POST['login']);
		$db->quote($un);
		$stmt->execute();
		$ps = $stmt->fetchAll();
		if (!empty($ps) && $ps[0]['password'] == $_POST['password'])
		{
			$_SESSION['login'] = $un;
			$_SESSION['uid'] = rand();
			
			$stmt = $db->prepare('SELECT password FROM admins WHERE username=:un');
			$stmt->bindParam(':un', $_POST['login']);
			$stmt->execute();
			$ps = $stmt->fetchAll();
			
			if (!empty($ps))
				$_SESSION['admin'] = true;
			
			header('Location: ./');
		}
		else
		{
			print('Неверный логин или пароль');
		}
	}
	catch(PDOException $e)
	{
		print($e->getMessage());
		exit();
	}
}
?>